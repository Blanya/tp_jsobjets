import Chien from "./Chien.js";

let bernie = new Chien("Bernie", "Bichon", 14);
let rex = new Chien("Rex", "Berger Allemand", 3);

let select = document.getElementById("select_content");
let id = document.getElementById("id_chien");
let info = document.getElementById("info");
let send = document.getElementById("btn_send");

select.options.add(new Option(bernie.name, bernie.id));
select.options.add(new Option(rex.name, rex.id));

select.addEventListener("change", getOption);

function getOption() 
{
    let valueOpt = this.options[select.selectedIndex].value;
    if( valueOpt != 0)
    {
        id.textContent = valueOpt;
        info.style.display = "block";
    }
    else
    {
        info.style.display = "none";
    }
}

send.addEventListener("click", addChien);

function addChien()
{
    let name = document.getElementById("name").value;
    let race = document.getElementById("race").value;
    let age = document.getElementById("age").value;

    let dog = new Chien(name, race, age);
    console.log(dog)
    select.options.add(new Option(dog.name, dog.id));
    select.options.selectedIndex = 0;
    info.style.display = "none";
}
