export default class Chien
{
    static compt = 0;
    constructor(name, race, age, id)
    {
        this.name = name;
        this.race = race;
        this.age = age;
        this.id = ++Chien.compt;
    }
}
