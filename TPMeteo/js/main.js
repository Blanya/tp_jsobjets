let city = document.getElementById("city");
let btnSearch = document.getElementById("btn_search");
let card = document.getElementById("info");
let cityContent = document.getElementById("city_content");
let degreContent = document.getElementById("degre");
let divWeather = document.getElementById("div_weather");
let humidityContent = document.getElementById("humidity");
let windContent = document.getElementById("wind");

let weather, wind, degre, humidity, icon;

btnSearch.addEventListener("click", affichage);

function recupererVille(ville)
{
    fetch("https://api.openweathermap.org/data/2.5/weather?q=" + ville + "&units=metric&appid=591a696c07e5ae1989772e1a93265390").then(response => response.json())
    .then(data => {
            weather = data.weather[0].description;
            wind = data.wind.speed;
            degre= data.main.temp;
            humidity = data.main.humidity;
            icon = data.weather[0].icon;
            console.log(data.weather);
            
            let imgBg = "https://source.unsplash.com/1600x900/?" + ville;
            document.body.style.backgroundImage = `url('${imgBg}')`;

            divWeather.innerHTML = `
                <img src="https://openweathermap.org/img/wn/${icon}.png"> <p>${weather}</p>
            `
            cityContent.textContent = ville;
            humidityContent.textContent = humidity
            degreContent.textContent = degre + " °C";
            windContent.textContent = wind;
        })
}

function affichage()
{
    let ville = city.value;
    recupererVille(ville);
}

recupererVille("Denver");