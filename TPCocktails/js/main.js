let info = document.getElementById("info");
let searchInput = document.getElementById("search_cocktail");

var liste, nb, listIngredient = [], ingredientName, search;

function getJson()
{
    fetch("https://www.thecocktaildb.com/api/json/v1/1/search.php?s=").then(response => response.json())
        .then(data => 
            {
                info.innerHTML = "";
                liste = data;
                for (let i = 0; i < liste.drinks.length; i++) 
                {
                    nb = i;
                    if(searchInput.value != "")
                    {
                        search = searchInput.value;
                        search = search[0].toUpperCase() + search.substring(1);   
                    }
                    
                    if(liste.drinks[i].strDrink.includes(searchInput.value.toUpperCase()) ||  liste.drinks[i].strDrink.includes(searchInput.value.toLowerCase()) || liste.drinks[i].strDrink.includes(search))
                    {
                        afficherCocktail(liste, nb);
                    }
                }

                    if(info.children.length == 0)
                    {
                        info.innerHTML = `<h2> Il n'y a aucun cocktail correspondant à votre recherche`
                    }
                displayCocktail(e)
            });
            
}


function afficherCocktail(liste, nb)
{
    info.innerHTML += `
            <div class="content_cocktail">
                <img src="${liste.drinks[nb].strDrinkThumb}" class="img_cocktail" onclick="displayCocktail(this)">
                <h2 class="title_cocktail">${liste.drinks[nb].strDrink}</h2>
            </div>
            `
}

getJson();

searchInput.addEventListener("keyup", getJson);

function displayCocktail(e)
{
    ingredientName = "";
    listIngredient = [];
    info.innerHTML = "";
    let title = e.parentNode.children[1].textContent;

    //capitalize
    titleModif = title[0].toUpperCase() + title.substring(1);

    for (let i = 0; i < liste.drinks.length; i++) 
    {
        if(liste.drinks[i].strDrink.includes(title.toUpperCase()) || liste.drinks[i].strDrink.includes(title.toLowerCase()) || liste.drinks[i].strDrink.includes(titleModif))
        {
            for (const [p, val] of Object.entries(liste.drinks[i])) {
                if (p.includes("strIngredient"))
                {
                    if(val != null)
                    {
                      listIngredient.push(val);
                    }
                }
            }
            
            for (let j = 0; j < listIngredient.length; j++) 
            {
                ingredientName += "<p class='ingredient'>🗹 " + listIngredient[j] + "</p>";
            }

            info.innerHTML += `
            <div id="content_one">
            <div class="content_cocktail-alone">
                <img src="${liste.drinks[i].strDrinkThumb}" class="img_cocktail-alone">
            </div>
            <div>
                <h2 class="title_cocktail-alone">${liste.drinks[i].strDrink}</h2>
                <p class="instruction">${liste.drinks[i].strInstructions}</p>
                <div> ${ingredientName} </div>
                <button class="button_all" onclick= "reset()">ALL COCKTAILS </button>
            </div>
            `;
            break;
        }
    }
}

function reset()
{
    searchInput.value = "";
    getJson();
}