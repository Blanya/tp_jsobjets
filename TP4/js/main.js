//import Post from './post.js';

let btnPost = document.getElementById("btn_post");
let postContainer = document.getElementById("post_container");
let divPost = document.querySelector(".div_post");
let postContent = document.getElementById("post_content");
let i = chargeItems();

btnPost.addEventListener("click", addPost);

function createDiv()
{
    let div = document.createElement("div");
    div = divPost.cloneNode(true);
    let text = postContent.value;
    let p = div.childNodes[1];
    p.textContent = text;
    postContent.value = " ";
    // console.log(div.childNodes[3].childNodes[1])
    // let btn = div.childNodes[3].childNodes[1];
    div.style.display = "flex";
    postContainer.appendChild(div);

    // add at local storage
    let objDiv = {
        content : text,
        id : i,
    };
    objDiv =  JSON.stringify(objDiv);
    localName = "div" + i;
    localStorage.setItem(localName,objDiv);
    i++;
}

function verifName()
{
    for( let nb = 0; nb < localStorage.length; nb++)
    {
        console.log(localStorage.key(nb));
        if(localStorage.key(nb) == "div" + nb )
        {
            return localName = localStorage.key(nb);
        }
        else return localName = "div" + i;
    }
}
function addPost()
{
    createDiv();
}

function modifPost(element)
{
    let parent = element.parentElement.parentElement;
    postContent.value = parent.childNodes[1].textContent;
    deletePost(element);
}

function deletePost(element)
{
    let parent = element.parentElement.parentElement;
    localStorage.removeItem('div' + element.value);
}

function chargeItems()
{
    if(localStorage.length != 0)
    {
        for(let j=1; j<=localStorage.length; j++)
        {   
            if(j==localStorage.length)
                {
                    return j;
                }
            else
            {
                let name = 'div' + j;
                let div = document.createElement("div");
                div = divPost.cloneNode(true);
                let objDiv = localStorage.getItem(name);
                let obj = JSON.parse(objDiv);
                let p = div.childNodes[1];
                div.childNodes[3].childNodes[1].value = obj.id;
                p.textContent = obj.content;
                div.style.display = "flex";
                postContainer.appendChild(div);
            }  
        }    
    }
    else
    {
        return 0;
    }
    
}
//post.innerhtml += ' ' => récupérer div

console.log(localStorage);