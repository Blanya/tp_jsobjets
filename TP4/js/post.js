export default class Post
{
    static count = 0;
    constructor (id, post)
    {
        this.id = ++Post.count
        this.post = post;
    }
}