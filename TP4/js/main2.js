let btnPost = document.getElementById("btn_post");
let postContainer = document.getElementById("post_container");
let divPost = document.querySelector(".div_post");
let postContent = document.getElementById("post_content");

btnPost.addEventListener("click", addPost);

function createDiv()
{
    let div = document.createElement("div");
    div = divPost.cloneNode(true);
    let text = postContent.value;
    let p = div.childNodes[1];
    p.textContent = text;
    postContent.value = " ";
    div.style.display = "flex";
    postContainer.appendChild(div);
}

function addPost()
{
    createDiv();
}

function modifPost(element)
{
    let parent = element.parentElement.parentElement;
    postContent.value = parent.childNodes[1].textContent;
    deletePost(element);
}

function deletePost(element)
{
    let parent = element.parentElement.parentElement;
    parent.remove();
}