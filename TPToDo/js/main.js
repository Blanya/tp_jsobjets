let openModal = document.getElementById("open_modal");
let modal = document.getElementById("modal");

//btn close and open modal
let closeModal = document.getElementById("modal_btn-close");
let closeBtn = document.querySelector(".btn_close");
let divClone = document.querySelector(".div_task");

//values
let title = document.getElementById("task_title");
let date = document.getElementById("date");
let description = document.getElementById("description");

//container
let taskContainer = document.getElementById("task");

//add task
let addTask = document.getElementById("modal_btn-add");
let modifyTask = document.getElementById("modal_btn-edit");


openModal.addEventListener("click", displayModal);
closeModal.addEventListener("click", hideModal);
closeBtn.addEventListener("click", hideModal);

addTask.addEventListener("click", createTask);

function displayModal()
{
    modal.classList.remove('none');
}

function displayModalModify()
{
    modifyTask.classList.remove("none");
    addTask.classList.add("none");
    displayModal();
}
function hideModal()
{
    title.value = "";
    description.value = "";
    date.value = "";
    modal.classList.add("none");
    addTask.classList.remove("none");
    modifyTask.classList.add("none");
}

function createTask()
{
    let titleTask = title.value;
    let dateTask = date.value;
    let descriptionTask = description.value;
    let div = document.createElement("div");
    div = divClone.cloneNode(true);
    div.children[0].textContent = titleTask;
    div.children[1].textContent = dateTask;
    div.children[2].textContent = descriptionTask;
    div.classList.remove("none");

    //define buttons
    const deleteBtn = div.querySelector(".delete");
    deleteBtn.addEventListener("click", deleteTask);
    const editBtn = div.querySelector(".modify");
    editBtn.addEventListener("click", editItem);

    taskContainer.appendChild(div);

    hideModal();

}

function deleteTask(element)
{
    let parent = element.currentTarget.parentElement.parentElement;
    parent.remove();
}

function editItem(element)
{
    let parent = element.currentTarget.parentElement.parentElement;
    title.value = parent.children[0].textContent;
    date.value = parent.children[1].textContent;
    description.value = parent.children[2].textContent;

    displayModalModify();
    modifyTask.addEventListener("click", () =>
    {
        parent.children[0].textContent = title.value;  
        parent.children[1].textContent = date.value;
        parent.children[2].textContent = description.value;

        //redefine 0
        element = " ";
        parent = " ";

        //change buttons 
        hideModal();
    });
}