import Person from "./Person.js";

let susan = new Person(
    1,
    "susan smith",
    "web developer",
    "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg","Développeur web diplômé d’une LP métiers de l’informatique ayant travaillé plus d’un an en tant qu’alternant puis employé d’une grande entreprise de sous-traitance de programmation web. Je souhaite évoluer dans votre entreprise et travailler sur vos projets dans le domaine de la course automobile car c’est ce qui me passionne.");

let anna = new Person(
    2,
    "anna johnson",
    "web designer",
    "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
    "Développeur full stack certifié Scrum, avec plus de 2 ans d’expérience. Mordu d’informatique, j’ai appris à coder dès mon plus jeune âge dans divers langages informatiques (Javascript, PHP, AngularJS...) et ai créé des projets personnels, dont un site référençant les vidéos les plus vues sur la plateforme Twitch (+ 5000 visites/mois)."
    );

let abdallah = new Person (
    3,
    "Abdallah Jones",
    "Web Developper",
    "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
    `Hello, je m'appelle Abdallah. J'aime le sport, les series et les animaux. Je suis développeur depuis plus de 10 ans.J'apprécie particulièrement le javascript et le Java. Je cherche actuellement une nouvelle opportunité pro`,
);

let bill = new Person (
    4,
    "bill anderson",
    "the boss",
    "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
    "Edison bulb put a bird on it humblebrag, marfa pok pok heirloom fashion axe cray stumptown venmo actually seitan. VHS farm-to-table schlitz, edison bulb pop-up 3 wolf moon tote bag street art shabby chic. "
);


let persons = [susan, anna, abdallah, bill];

let cardName = document.getElementById("card_name");
let cardJob = document.getElementById("card_job");
let cardImg = document.getElementById("card_img");
let cardDescription = document.getElementById("card_job-description");
let prevBtn = document.getElementById("prev");
let nextBtn = document.getElementById("next");
let surpriseBtn = document.getElementById("surprise");

cardName.textContent = susan.name;
cardJob.textContent = susan.job;
cardImg.setAttribute("src", susan.img);
cardDescription.textContent = susan.text;
let id = susan.id;

prevBtn.addEventListener("click", prev);
nextBtn.addEventListener("click", next);
surpriseBtn.addEventListener("click", surprise);

function affichage()
{
    persons.forEach( person =>{
        if(person.id == id)
        {
            cardName.textContent = person.name;
            cardJob.textContent = person.job;
            cardImg.setAttribute("src", person.img);
            cardDescription.textContent = person.text;
        }
    });
}

function prev()
{
    if(id == 1)
    {
        id = persons.length;
    }
    else 
    {
        id -= 1;
    }
    affichage();
}

function next()
{
    if(id == 4)
    {
        id = 1;
    }
    else 
    {
        id += 1;
    }
    affichage();
}

function surprise()
{
    //get Random =! 0
    function getRandom(min, max)
    {
        return Math.floor(Math.random() * (max) + min);
    }

    do
    {
        var random = getRandom(1, persons.length);
    }while(id == random);
    console.log(random)
    id = random;
    affichage();
}