let divAddEleve = document.getElementById("div_add-eleve");
let divAddMatiere = document.getElementById("div_add-matiere");
let divAddNote = document.getElementById("div_add-note");

function display(element)
{
    let atDisplaying = element.parentElement.parentElement.children[2];
    if(atDisplaying.className == "none")
    {
        element.textContent = "ON";
        atDisplaying.className = " ";
    }
    else
    {
        element.textContent = "OFF";
        atDisplaying.className = "none";
    }
}