import Mammifere from "./Mammifère.js";

export default class Chien extends Mammifere
{
    Respiration()
    {
        console.log("Inspiration, Expiration, j'ai des poumons..");
    }

    Alimentation()
    {
        console.log("Je mange des os et des croquettes");
    }

    Aboyer()
    {
        if(this.heartRate)
        {
            console.log("Wouf");
        }
        else
        {
            console.log("Je ne suis plus ..");
        }
    }
}