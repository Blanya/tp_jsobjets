import Chien from "./Chien.js";
import Fleur from "./Fleur.js";
import Person from "./Person.js";

// let medor = new Chien("Medor", "Berger Allemand", 12);
// let milou = new Chien("Milou", "Pitbull");
// let titou = new Chien();

// medor.Aboyer();

// console.log("Le nom du chien est : " + medor.nom + " et il a " + medor.age);

// medor.age = 45;
// console.log(medor.age);
// console.log(titou);
// titou.nom = "Titou";
// console.log(titou);

// let orchidee = new Fleur("Beaute", "Orchidée");
// console.log(orchidee);

// let etresVivants = [medor, milou, orchidee, titou];

// for (const etre of etresVivants) 
// {
//     console.log("-----" + etre.nom + "-----");
//     etre.Respiration();
//     etre.Localisation();
//     etre.Mort();
//     console.log("------------");
// }

// let listeChien = etresVivants.filter(etre => etre instanceof Chien);

// //affichage Tableau
// console.log(listeChien); 

// listeChien.forEach(x => console.log(x.nom));

// //retourne le premier chien
// let chien = etresVivants.find(etre => etre instanceof Chien);
// console.log(chien);

// let fleur = etresVivants.find(etre => etre instanceof Fleur);
// console.log(fleur);


// //Map pour modifer le tableau   
// // etresVivants.map((etre)=> {
// //     etre.nom = etre.nom + " lol ";
// // });

// etresVivants.forEach(x=> console.log(x.nom));

// etresVivants.forEach(etre => {
//     if(etre.nom.startsWith('M'))
//     {
//         console.log(etre);
//     }
//     if(etre.type === "Berger Allemand")
//     {
//         console.log("Je suis un berger Allemand");
//     }
// })


// const hero = {
//     name: "Batman",
//     realName: "Bruce Wayn"
// };

// const {name, realName} = hero;

// console.log(hero.name);
// console.log(name);

//let person1 = new Person("Louis", "Michel", 32, "France", "Béthune");
let person2 = new Person("John", "Smith", 22, "USA", "N-Y");

// console.log(person1.id);
// console.log(person2.id);

// console.log(person1.setScore = 2)
// console.log(person1.getScore)

console.log(Person.favoriteSkills());
console.log(Person.favoriteSkills());

//localStorage.setItem("person", JSON.stringify(person1));
//let person5 = JSON.parse(localStorage.getItem("person"));

// document.getElementById("nom").textContent = person5.lastName;
// document.getElementById("prenom").textContent = person5.firstName;
// document.getElementById("age").textContent = person5.age;
// document.getElementById("pays").textContent = person5.country;
// document.getElementById("ville").textContent = person5.city;


var val = true;

const verif = val == true ? "bon" : "pas bon";

for (let nb = 1; nb < 50; nb++) 
{
    const resultat = nb%3 == 0 & nb%5 == 0 ? "fizzBuzz" : nb%3==0 ? "fizz" : nb%5==0 ? "buzz" : nb ;   
    
    console.log(resultat);
}
