export default class Voiture
{
    constructor(marque, modèle, vitesse)
    {
        this.marque = marque;
        this.modèle = modèle;
        this.vitesse = vitesse;
    }

    set setTourner(vitesse)
    {
        this.vitesse -= 5;
    }
    get getTourner()
    {
        return this.vitesse;
    }

    set setAcceleration(vitesse)
    {
        this.vitesse += 10;
    }
    get getAcceleration()
    {
        return this.vitesse;
    }
}