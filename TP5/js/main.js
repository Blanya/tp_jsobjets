let btnSend = document.getElementById("submit_btn");
let btnEdit = document.getElementById("edit_btn");
let cardContainer = document.getElementById("list_container");
let divItem = document.querySelector(".item_content");
let inputValue = document.getElementById("input_text");
//phrases informatives
let valueChange = document.getElementById("modif_content");
let addContent = document.getElementById("add_content");
let removeItem = document.getElementById("remove_content");
let removeAllItems = document.getElementById("remove_all_content");

let i = chargeItems();


btnSend.addEventListener("click", addItem);

function addItem()
{
    //add div
    let div = document.createElement("div");
    div = divItem.cloneNode(true);
    div.classList.remove("none");
    p = div.children[0];
    let text = inputValue.value;
    p.textContent = text;
    inputValue.value = "";
    let btnValue = div.childNodes[3].childNodes[1];
    btnValue.value = i;
    cardContainer.appendChild(div);
    

    //add in local storage
    localName = "item" + i;
    localStorage.setItem(localName, text);
    i++;

    //affichage ajout d'un élément
    addContent.classList.remove("none");
}

function modify(element)
{
    let parent = element.parentElement.parentElement;
    inputValue.value = parent.childNodes[1].textContent;
    btnSend.classList.add("none");
    btnEdit.classList.remove("none");
    btnEdit.addEventListener("click", () =>
    {
        let parent = element.parentElement.parentElement;
        parent.children[0].textContent = inputValue.value;
        let nbItem = element.parentElement.childNodes[1].value;
        let nameItem = "item" + nbItem;
        localStorage.setItem(nameItem, inputValue.value);
        btnEdit.classList.add("none");
        btnSend.classList.remove("none");  
        element = " ";  
        valueChange.classList.remove("none");
    })
}

function remove(element)
{
    let parent = element.parentElement.parentElement;
    parent.remove();
    let nbItem = element.parentElement.childNodes[1].value;
    localStorage.removeItem("item" + nbItem);
    removeClass();
    removeItem.classList.remove("none");
    while(cardContainer.firstChild)
    {
        cardContainer.removeChild(cardContainer.firstChild);
    };
    chargeItems();
}

function removeClass()
{
    removeItem.classList.add("none");
    removeAllItems.classList.add("none");
    valueChange.classList.add("none");
    addContent.classList.add("none");
}

function removeAllContent()
{
    while(cardContainer.firstChild)
    {
        cardContainer.removeChild(cardContainer.firstChild);
    }
    localStorage.clear();
    chargeItems();
    removeClass();
    removeAllItems.classList.remove("none");
}

function chargeItems()
{
    j = 0;

    if(localStorage.length != 0)
    {
        for (let i = 0; i <= localStorage.length; i++) 
        {
            if( i == localStorage.length)
            {
                return i;
            }
            else
            {
                let div = document.createElement("div");
                div = divItem.cloneNode(true);
                div.classList.remove("none");
                let text = (localStorage.getItem(localStorage.key(i)));
                let p = div.childNodes[1];
                let btnValue = div.childNodes[3].childNodes[1];
                btnValue.value = (localStorage.key(i)).substring(4);
                console.log(btnValue)
                p.textContent = text;
                cardContainer.appendChild(div);  
            } 
        }
    }
    else 
    {
        return j;
    }
}

console.log(localStorage);